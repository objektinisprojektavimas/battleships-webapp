package battleships.webapp;

import battleships.webapp.enums.NumberOfShotsPerTurnEnum;
import battleships.webapp.models.GameSettings;
import battleships.webapp.models.GameStartState;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BattleshipsWebappApplication {

	public static void main(String[] args) {
		SpringApplication.run(BattleshipsWebappApplication.class, args);
	}

	@Bean
	public GameSettings gameSettings() {
		return new GameSettings("10", "10", NumberOfShotsPerTurnEnum.FIXED_NUMBER_OF_SHOTS.getText());
	}
}
