package battleships.webapp.controllers;

import battleships.webapp.models.GameSettings;
import battleships.webapp.models.GameStartState;
import battleships.webapp.services.ConnectionService;
import battleships.webapp.services.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/game")
public class GameController {

    @Autowired
    private GameService gameService;

    @Autowired
    private ConnectionService connectionService;

    @PostMapping(value = "/start", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity startGame(@RequestBody GameSettings gameSettings, @RequestParam String id) {
        if (!gameService.getGameSettings().equals(gameSettings)) {
            connectionService.resetAgreedToStartClients();
        }
        if (gameService.saveSettings(gameSettings) && connectionService.start(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "/settings", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GameSettings> getGameSettings() {
        return new ResponseEntity<>(gameService.getGameSettings(), HttpStatus.OK);
    }

    @GetMapping(value = "/start/state", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GameStartState> getGameStartState() {
        return new ResponseEntity<>(connectionService.getGameStartState(), HttpStatus.OK);
    }
}
