package battleships.webapp.controllers;

import battleships.webapp.models.Client;
import battleships.webapp.services.ConnectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/connect")
public class ConnectionController {

    @Autowired
    private ConnectionService connectionService;

    @PostMapping(value = "/lobby", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Client> connectToLobby(@RequestBody String name, HttpServletRequest request) {
        if (connectionService.isNameUnique(name)) {
            Client client = connectionService.createClient(name, request.getRemoteAddr());
            return new ResponseEntity<>(client, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "/clients", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Client> getAllClients() {
        return connectionService.getClients();
    }

    @PostMapping(value = "/ping", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity ping(@RequestBody Client client) {
        System.out.println(client.getName());
        connectionService.ping(client);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
