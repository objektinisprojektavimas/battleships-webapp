package battleships.webapp.models;

import battleships.webapp.enums.NumberOfShotsPerTurnEnum;

import java.util.Objects;

public class GameSettings {

    private String rows;
    private String columns;
    private String numberOfShotsPerTurn;

    public GameSettings(String rows, String columns, String numberOfShotsPerTurn) {
        this.rows = rows;
        this.columns = columns;
        this.numberOfShotsPerTurn = numberOfShotsPerTurn;
    }

    public GameSettings() {
    }

    public String getRows() {
        return rows;
    }

    public void setRows(String rows) {
        this.rows = rows;
    }

    public String getColumns() {
        return columns;
    }

    public void setColumns(String columns) {
        this.columns = columns;
    }

    public String getNumberOfShotsPerTurn() {
        return numberOfShotsPerTurn;
    }

    public void setNumberOfShotsPerTurn(String numberOfShotsPerTurn) {
        this.numberOfShotsPerTurn = numberOfShotsPerTurn;
    }

    public void load(GameSettings settings) {
        this.columns = settings.getColumns();
        this.rows = settings.getRows();
        this.numberOfShotsPerTurn = settings.getNumberOfShotsPerTurn();
    }

    public static boolean isValid(GameSettings settings) {
        try {
            Integer.parseInt(settings.getRows());
            Integer.parseInt(settings.getColumns());
        } catch (Exception e) {
            return false;
        }

        if (settings.getNumberOfShotsPerTurn().equals(NumberOfShotsPerTurnEnum.FIXED_NUMBER_OF_SHOTS.getText()) ||
                settings.getNumberOfShotsPerTurn().equals(NumberOfShotsPerTurnEnum.NUMBER_OF_SHOTS_BY_UNSUNKEN_SHIPS.getText()) ||
                settings.getNumberOfShotsPerTurn().equals(NumberOfShotsPerTurnEnum.NUMBER_OF_SHOTS_BY_LARGEST_SHIP.getText())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameSettings that = (GameSettings) o;
        return Objects.equals(rows, that.rows) &&
                Objects.equals(columns, that.columns) &&
                Objects.equals(numberOfShotsPerTurn, that.numberOfShotsPerTurn);
    }

    @Override
    public int hashCode() {

        return Objects.hash(rows, columns, numberOfShotsPerTurn);
    }
}
