package battleships.webapp.models;

import java.util.ArrayList;
import java.util.List;

public class GameState {

    private static List<Client> clients = new ArrayList<>();
    private static List<Client> clientsThatWantToStart = new ArrayList<>();
    private static GameSettings gameSettings;

    public static List<Client> getClients() {
        return clients;
    }

    public static void setClients(List<Client> clients) {
        GameState.clients = clients;
    }

    public static List<Client> getClientsThatWantToStart() {
        return clientsThatWantToStart;
    }

    public static void setClientsThatWantToStart(List<Client> clientsThatWantToStart) {
        GameState.clientsThatWantToStart = clientsThatWantToStart;
    }

    public static GameSettings getGameSettings() {
        return gameSettings;
    }

    public static void setGameSettings(GameSettings gameSettings) {
        GameState.gameSettings = gameSettings;
    }
}
