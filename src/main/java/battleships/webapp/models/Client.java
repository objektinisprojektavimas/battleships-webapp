package battleships.webapp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Objects;

public class Client {

    @JsonIgnore
    private long lastUpdated;

    private String id;
    private String ip;
    private String name;
    private static final String SEPARATOR = ":";

    public Client() {
    }

    public Client(String ip, String name) {
        this.ip = ip;
        this.name = name;
        this.id = createId(ip, name);
        this.lastUpdated = System.currentTimeMillis();
    }

    private String createId(String ip, String name) {
        return ip + SEPARATOR + name;
    }

    public String getId() {
        return id;
    }

    public String getIp() {
        return ip;
    }

    public String getName() {
        return name;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return lastUpdated == client.lastUpdated &&
                Objects.equals(id, client.id) &&
                Objects.equals(ip, client.ip) &&
                Objects.equals(name, client.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastUpdated, id, ip, name);
    }
}
