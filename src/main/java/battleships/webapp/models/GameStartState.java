package battleships.webapp.models;

public class GameStartState {

    private static final String STARTED = "STARTED";
    private static final String NOT_STARTED = "NOT_STARTED";

    private String state;
    private int allClients;
    private int agreedClients;

    public GameStartState(int allClients, int agreedClients) {
        this.allClients = allClients;
        this.agreedClients = agreedClients;
        this.state = mapGameState(allClients, agreedClients);
    }

    public String getState() {
        return state;
    }

    public int getAllClients() {
        return allClients;
    }

    public int getAgreedClients() {
        return agreedClients;
    }

    private String mapGameState(int allClients, int agreedClients) {
        if (allClients == 0 || agreedClients == 0) {
            return NOT_STARTED;
        }

        if (agreedClients < allClients) {
            return NOT_STARTED;
        }

        if (agreedClients == allClients) {
            return STARTED;
        }

        return NOT_STARTED;
    }
}
