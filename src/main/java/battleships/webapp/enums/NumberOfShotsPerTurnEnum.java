package battleships.webapp.enums;

public enum NumberOfShotsPerTurnEnum {
    FIXED_NUMBER_OF_SHOTS("FIXED_NUMBER_OF_SHOTS"),
    NUMBER_OF_SHOTS_BY_UNSUNKEN_SHIPS("NUMBER_OF_SHOTS_BY_UNSUNKEN_SHIPS"),
    NUMBER_OF_SHOTS_BY_LARGEST_SHIP("NUMBER_OF_SHOTS_BY_LARGEST_SHIP");

    private String text;

    NumberOfShotsPerTurnEnum(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
