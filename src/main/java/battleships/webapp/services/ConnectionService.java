package battleships.webapp.services;

import battleships.webapp.models.Client;
import battleships.webapp.models.GameStartState;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ConnectionService {

    private List<Client> clients = new ArrayList<>();
    private List<Client> agreedToStartClients = new ArrayList<>();
    private long clientTimeout = 5000;

    public boolean isNameUnique(String name) {
        return !clients.stream().anyMatch(client -> client.getName().equals(name));
    }

    public Client createClient(String name, String ip) {
        Client client = new Client(ip, name);
        clients.add(client);

        return client;
    }

    public List<Client> getClients() {
        return this.clients;
    }

    private Optional<Client> findClientById(List<Client> clients, String id) {
        return clients.stream()
                .filter(client -> client.getId().equals(id))
                .findFirst();
    }

    private void updateClientTime(Client client) {
        client.setLastUpdated(System.currentTimeMillis());
    }

    private void clientsCleanup() {
        clients.removeIf(client -> System.currentTimeMillis() - client.getLastUpdated() > clientTimeout);
    }

    private void cleanupStartedClients() {
        agreedToStartClients.removeIf(client -> !clients.contains(client));
    }

    public void ping(Client client) {
        findClientById(clients, client.getId()).ifPresent(this::updateClientTime);
        clientsCleanup();
        cleanupStartedClients();
    }

    public boolean start(String id) {
        Optional<Client> client = findClientById(clients, id);
        if (client.isPresent() && !findClientById(agreedToStartClients, id).isPresent()) {
            agreedToStartClients.add(client.get());
            return true;
        } else {
            return false;
        }
    }

    public void resetAgreedToStartClients() {
        this.agreedToStartClients.clear();
    }

    public GameStartState getGameStartState() {
        return new GameStartState(clients.size(), agreedToStartClients.size());
    }
}
