package battleships.webapp.services;

import battleships.webapp.models.GameSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameService {

    @Autowired
    private GameSettings gameSettings;

    public boolean saveSettings(GameSettings gameSettings) {
        if (GameSettings.isValid(gameSettings)) {
            this.gameSettings.load(gameSettings);
            return true;
        } else {
            return false;
        }
    }

    public GameSettings getGameSettings() {
        return gameSettings;
    }
}
