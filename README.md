# Battleships WebApp

Instructions on how to setup and run battleships-webapp on your local environment:

Required installations:
  - Java 8
  - Maven
  - IntelliJ IDEA

Setup environment variables:
  - *JAVA_HOME*: your java instalation directory (e. g. *C:\Program Files\Java\jdk1.8.0_144*)
  - Edit *Path* variable by adding maven /bin instalation directory path (e. g. *C:\Users\tools\apache-maven-3.5.2\bin*)

Steps:
1. Clone repository to your local machine
2. Open IntelliJ and click *Import Project* -> select *pom.xml* -> click *OK* -> no more changes should be needed so click *next* and *finish*
3. Wait for IntelliJ to open the project
4. Go to directory of project in Command Line Tool like 'Command Prompt' (e. g. cd *C:\Users\name\intellij-wokspace\battleships\battleships-webapp*)
5. type *mvn clean install* to build the project
6. If the build was successful go to IntelliJ and try running the app by clicking 'Run' -> 'BattleshipsWebappApplication'
7. If you see in the console output something like this *Started BattleshipsWebappApplication in 3.32 seconds (JVM running for 4.388)* the app is running
8. Try web API by going to *http://localhost:8080/meta/healthcheck* url in the internet browser, if you see *Battleships Web App is Running* then everything is OK.